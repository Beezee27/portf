<?php

//Allow for a custom menu added through wordpress back-end.
function brad_custom_menu() {
  register_nav_menu('Brad-custom-menu',__('Brad custom menu'));
}
add_action('init', 'brad_custom_menu');


//Attach all scripts and style sheets to theme
function portfolio_style_script() {
  wp_enqueue_style ('custom', get_template_directory_uri() . '/style.css');
  wp_enqueue_style('hamburger', get_template_directory_uri() . '/hamburger/hamburger.css');

  wp_enqueue_style('hamburgerMIN', get_template_directory_uri() . '/hamburger/hamburger.min.css');

  wp_enqueue_script( 'hamburgerJS', get_template_directory_uri() . '/hamburger/hamburger.js', array( 'jquery' ), '1.0.0', true );


}
add_action( 'wp_enqueue_scripts', 'portfolio_style_script' );


 ?>
