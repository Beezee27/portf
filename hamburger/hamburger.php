<div class="topnav" id="myTopnav">
  <img class="branded1" src=" <?php echo $brand_logo['url']; ?>" alt="" />
  <div class="hidehamburger">
    <button class="hamburger hamburger--collapse" type="button">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>
  </div>

  <?php
wp_nav_menu( array(
    'menu' => 'menu-1',
    'container_class' => 'custom-menu-class' ) );
?>
</div>
