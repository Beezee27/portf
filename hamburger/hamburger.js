jQuery(document).ready(function()
{
  /*
  Here I'm checking the size of the window & only setting a display none if the window width is <= 768 (iPad)
  */
  if(jQuery(window).width() <= 768)
  {
    var $menu = jQuery(".custom-menu-class #menu-main-menu");

    jQuery(".hamburger").click(function()
    {
      if(!$menu.hasClass("toggled"))
      {
        jQuery(this).addClass("is-active");
        $menu.slideDown().addClass("toggled");

      }
      else
      {
        jQuery(this).removeClass("is-active");
        $menu.slideUp().removeClass("toggled");
      }
    });
  }
});
 
